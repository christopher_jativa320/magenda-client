import { Component, OnInit } from '@angular/core';
import { ChatService } from './services/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Magenda';
  message: String = '';
  server_message: String = '';

  constructor(private chat: ChatService) {}

  ngOnInit() {
    this.chat.messages.subscribe(msg => {
      this.server_message = msg.text;
    });
  }

  onButtonClick() {
    this.chat.sendMsg(this.message);
  }
}
