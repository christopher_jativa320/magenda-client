import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';
import { Observable, Subject } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  messages: Subject<any>;

  // Our construct uses the connect method of our WebsocketService object
  constructor(private ws: WebsocketService) { 
    this.messages = <Subject<any>>ws
    .connect()
    .map((response: any): any=> {
      return response;
    })
  }

  // Our interface for sending messages back to the socket.io server
  sendMsg(msg) {
    this.messages.next(msg);
  }
}
