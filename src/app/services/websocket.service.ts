import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import * as Rx from 'rxjs/Rx';


@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  // Our socket connection object
  private socket;

  constructor() { }

  connect(): Rx.Subject<MessageEvent> {
    // Where our server side socket connection is listening on
    this.socket = io('http://localhost:8080');

    // Define our observable which will observe for any incoming messages from the socket.io server
    let observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        console.log('Received message from Websocket Server');
        observer.next(data);
      })
      return () => {
        this.socket.disconnect();
      }
    });

    // Define our Observer which listens for messages from our other components and sends messages back to the socket server whenever the 'next()' method is called
    let observer = {
      next: (data: Object) => {
        this.socket.emit('message', JSON.stringify(data));
      },
    };

    // Return our Rx.Subject which is a combination of both an observer and observable
    return Rx.Subject.create(observer, observable);
  }
}